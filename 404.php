<?php get_header(); ?>

<img src="/wp-content/uploads/page-bg.jpg" width="100%" />
			
	<div id="content">

		<div id="inner-content" class="row">
	
			<main id="main" class="large-12 medium-12 columns" role="main">

				<article id="content-not-found">
				
					<header class="article-header">
						<h1><?php _e( 'Oh Flute!', 'jointswp' ); ?></h1>
					</header> <!-- end article header -->
			
					<section class="entry-content">
						<p><?php _e( 'Whatever you were looking for must be hiding... try looking again!', 'jointswp' ); ?></p>
					</section> <!-- end article section -->

					<section class="search">
					    <p><?php get_search_form(); ?></p>
					</section> <!-- end search section -->
			
				</article> <!-- end article -->
	
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>