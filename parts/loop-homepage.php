<article id="homepage-text" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
						
	<div class="homepage-text-parallax"></div>
				
    <section class="entry-content" itemprop="articleBody">
	    <?php the_content(); ?>
	</section> <!-- end article section -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->