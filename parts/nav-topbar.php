<div class="very-top-bar">
    <div class="row">
        <div class="large-8 columns leftside visible-for-large-up">
            <h3 class="top-message">Welcome to Marching World</h3>
            <ul class="hide-for-small-only">
                <li><a href="/estimates/">Estimates</a></li>
                <li><a href="/terms-conditions/">Terms</a></li>
                <li><a href="#newsletter">Newsletter</a></li>
            </ul>
        </div>
        <div class="medium-8 small-10-small columns leftside hide-for-large-up">
        	<h3 class="top-message"><?php echo get_option('my_field'); ?></h3>
        	<ul>
        	<li><a href="tel:8007332273"><i class="fa fa-phone"><span>Call Us</span></i></a></li>
        	</ul>
        </div>
        <div class="large-4 columns rightside visible-for-large-up">
            <ul>
                <li><a href="/my-account/">My Account</a></li>
                <li><a href="/checkout/">Checkout</a></li>
            </ul>
        </div>
        <div class="medium-4 small-2-big columns rightside hide-for-large-up">
            <ul>
                <li><a href="/my-account/"><i class="fa fa-user"><span>My Account</span></i></a></li>
                <li><a href="/checkout/"><i class="fa fa-shopping-cart"><span>Chckout</span></i></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="top-bar" id="main-menu">
	<div class="row">
		<div class="large-2 columns logo">
			<a href="/"><img src="/wp-content/uploads/marching-world-logo.png" id="site-logo" /></a>
		</div>
		<div class="small-12 columns small-logo">
 			<div class="header_search_container">
                    <form autocomplete="off" action="//www.marchingworld.com/site-search/" method="get" class="header_search_widget">
                            <input type="text" onblur="if (this.value == '') {this.value = 'Search';}" onfocus="if (this.value == 'Search') {this.value = '';}" value="Search" name="rs" class="header_search_input" 
                            data-ps-id="3536"
                            data-ps-default_text="Search" 
                            data-ps-row="22" 
                            data-ps-text_lenght="100" 
                            data-ps-cat_in="all"
                                        data-ps-popup_search_in="{&quot;product&quot;:&quot;10&quot;,&quot;p_sku&quot;:&quot;10&quot;,&quot;p_cat&quot;:&quot;&quot;,&quot;p_tag&quot;:&quot;&quot;,&quot;post&quot;:&quot;&quot;,&quot;page&quot;:&quot;&quot;}"             data-ps-search_in="product"             data-ps-search_other="product,p_sku"             data-ps-show_price="1" 
                            />
                            <span class="header_search_bt"></span>
                        <input type="hidden" name="search_in" value="product"  />
                        <input type="hidden" name="cat_in" value="all"  />
                        <input type="hidden" name="search_other" value="product,p_sku"  />
                    </form>
                </div>
			<a href="/"><img src="/wp-content/uploads/marching-world-logo.png" id="site-logo" /></a>
			<a class="toggleness" data-toggle="off-canvas"><i class="fa fa-bars"></i></a>
		</div>
		<div class="large-10 medium-10 small-12 columns text-right">
			<h3>Need Help?</h3>
			<h4><?php echo get_option('hours_field'); ?></h4>
			<h2><a href="tel:8007332273">(800) 733-BAND</a></h2>

			<div class="main-menu">
				<?php joints_top_nav(); ?>
 				<div class="header_search_container">
                    <form autocomplete="off" action="//www.marchingworld.com/site-search/" method="get" class="header_search_widget">
                            <input type="text" onblur="if (this.value == '') {this.value = 'Search';}" onfocus="if (this.value == 'Search') {this.value = '';}" value="Search" name="rs" class="header_search_input" 
                            data-ps-id="3536"
                            data-ps-default_text="Search" 
                            data-ps-row="22" 
                            data-ps-text_lenght="100" 
                            data-ps-cat_in="all"
                                        data-ps-popup_search_in="{&quot;product&quot;:&quot;10&quot;,&quot;p_sku&quot;:&quot;10&quot;,&quot;p_cat&quot;:&quot;&quot;,&quot;p_tag&quot;:&quot;&quot;,&quot;post&quot;:&quot;&quot;,&quot;page&quot;:&quot;&quot;}"             data-ps-search_in="product"             data-ps-search_other="product,p_sku"             data-ps-show_price="1" 
                            />
                            <span class="header_search_bt"></span>
                        <input type="hidden" name="search_in" value="product"  />
                        <input type="hidden" name="cat_in" value="all"  />
                        <input type="hidden" name="search_other" value="product,p_sku"  />
                    </form>
                </div>
			</div>
		</div>		
	</div>
</div>