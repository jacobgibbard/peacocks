<?php get_header(); ?>

<img src="/wp-content/uploads/page-bg.jpg" width="100%" />
			
<div id="content">

	<div id="inner-content" class="row">

		<main id="main" class="large-9 medium-9 columns woocommercewrapped" role="main">
		
		    <?php if ( have_posts() ) :?>
		
		    	<?php woocommerce_content(); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

		<?php get_sidebar(); ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>