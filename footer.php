					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="row">
							<div class="large-4 medium-4 columns">
								<ul>
									<a href="/"><img src="/wp-content/uploads/logo.png" /></a>
									<li>Peacocks Marching World has been serving Marching Bands, Colorguards, Dance/Drill teams, Drum Majors and the Entertainment Industry since 1969.</li>
									<li>325 Rutherford St. Suite D<br />
									Goleta, CA 93117<br />
									<a href="tel:8007332273"><strong>(800) 733-BAND</strong></a><br />
									Local: <a href="tel:8058452427">(805) 845-2427</a><br />
									Fax: (805) 456-0296</li>
								</ul>
		    				</div>
		    				<div class="large-2 medium-2 columns">
								<div class="data-block-one">
									<h6 class="trigger">Information</h6>
			 						<ul class="data-collapse">
										<li><a href="/about/">About Us</a></li>
										<li><a href="/blog/">Blog</a></li>
										<li><a href="/links/">Links</a></li>
										<li><a href="/privacy-policy/">Privacy Policy</a></li>
										<li><a href="/contact/">Contact Us</a></li>
										<li><a href="/sitemap/">Sitemap</a></li>
										<li><a href="/terms-conditions/">Terms &amp; Conditions</a></li>
									</ul>
								</div>
		    				</div>
		    				<div class="large-2 medium-2 columns">
								<div class="data-block-two">
									<h6 class="trigger">My Account</h6>
									<ul class="data-collapse">
										<li><a href="/my-account/">My Account</a></li>
										<li><a href="/estimates/">Estimates</a></li>
										<li><a href="/shop/">Shop</a></li>
										<li><a href="/cart/">Cart</a></li>
										<li><a href="/checkout/">Checkout</a></li>
									</ul>
								</div>
		    				</div>
		    				<div class="large-3 medium-4 columns">
								<ul>
									<li><h6>Join Our Newsletter</h6><a name="newsletter"></a></li>
									<li>
										<form class="newsletterfooter" action="http://marchingworld.us7.list-manage1.com/subscribe/post" method="POST">
											<input type="hidden" name="u" value="08159445228830afa903d1605">
											<input type="hidden" name="id" value="87e926cb09">
											<input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="" placeholder="Enter email and push return">
											<input type="text" name="b_08159445228830afa903d1605_87e926cb09" tabindex="-1" value="" style="display:none">
											<input type="submit" class="button" name="submit" value="Subscribe to list" style="display:none">
										</form>
									</li>
									<li>Stay Connected</li>
									<li><a href="http://www.facebook.com/marchingworld/" target="_blank"><i class="fa fa-facebook-square"></i></a> <a href="http://www.instagram.com/marchingworld" target="_blank"><i class="fa fa-instagram"></i></a></li>
								</ul>
		    				</div>
							<div class="large-12 medium-12 columns">
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> | All Rights Reserved | Site by <a href="http://www.gibbardwebdesign.com/" target="_blank">GWD</a></p>
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->

		<?php wp_footer(); ?>
		
	</body>
</html> <!-- end page -->