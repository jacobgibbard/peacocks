<?php
/*
Template Name: Homepage
*/
get_header(); ?>

<?php putRevSlider( 'homepage' ); ?>

	<div id="featured-categories">
		<div id="category-tiles" class="row">
			<div class="large-12 medium-12 small-12 columns">
				
				<div class="category-tile">
					<a href="/product-category/marching-band/">
						<img src="/wp-content/uploads/marchingband-tile.jpg" />
						<div class="info">
						    <h4>Marching Band</h4>
						</div>
					</a>
				</div>

				<div class="category-tile">
					<a href="/product-category/colorguard/">
						<img src="/wp-content/uploads/colorguard-tile.jpg" />
						<div class="info">
						    <h4>Colorguard</h4>
						</div>
					</a>
				</div>

				<div class="category-tile">
					<a href="/product-category/drum-major/">
						<img src="/wp-content/uploads/drummajor-tile.jpg" />
						<div class="info">
						    <h4>Drum Major</h4>
						</div>
					</a>
				</div>

				<div class="category-tile">
					<a href="/product-category/drumline/">
						<img src="/wp-content/uploads/drumline-tiled.jpg" />
						<div class="info">
						    <h4>Drumline</h4>
						</div>
					</a>
				</div>

				<div class="category-tile">
					<a href="/product-category/concert-band/">
						<img src="/wp-content/uploads/concertband-tile.jpg" />
						<div class="info">
						    <h4>Concert Band</h4>
						</div>
					</a>
				</div>

				<div class="category-tile">
					<a href="/product-category/military-rotc/">
						<img src="/wp-content/uploads/rotc-tile.jpg" />
						<div class="info">
						    <h4>Military/Rotc</h4>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div>
	
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns featured-products" role="main">
				
					<h4>Sales and Specials</h4>
					<h3>Get Ready For Summer</h3>
					<?php  echo do_shortcode('[yith_wc_productslider id="97212"]'); ?>	
			    					
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php get_template_part( 'parts/loop', 'homepage' ); ?>
			    
		<?php endwhile; endif; ?>	

		<div class="homepage-parallax">
			
			<div class="row">
				<div class="large-12 columns">
					<h3>Customer Service</h3>
					<p>We're here when you need us.</p>
					<a href="/contact/">Contact Us</a>
				</div>
			</div>

		</div>

		<div id="bottom-content" class="row">

			<div class="bottom-content-header">
				<h3>Trending</h3>
				<a href="http://www.facebook.com/marchingworld/" target="_blank" class="facebook-icon"><i class="fa fa-facebook-square"></i></a>
			</div>

			<?php echo do_shortcode('[custom-facebook-feed]'); ?>

		</div>

	</div> <!-- end #content -->

<?php get_footer(); ?>